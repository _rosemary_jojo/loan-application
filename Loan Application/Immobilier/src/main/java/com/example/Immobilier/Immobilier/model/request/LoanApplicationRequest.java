package com.example.Immobilier.Immobilier.model.request;

import com.example.Immobilier.Immobilier.constant.SalaryRange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanApplicationRequest {

    public String name;
    public int age;
    public String national_id;
    public float loan_amt;
    public int loan_tenure;



}

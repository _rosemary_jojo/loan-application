package com.example.Immobilier.Immobilier.model.response;

import com.example.Immobilier.Immobilier.constant.LoanApplicationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoanApplicationResponseStatus {

    public int id;
    public LoanApplicationStatus  status;
}

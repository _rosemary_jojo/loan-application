package com.example.Immobilier.Immobilier.controller;

import com.example.Immobilier.Immobilier.constant.CountryEnum;
import com.example.Immobilier.Immobilier.constant.SalaryRange;
import com.example.Immobilier.Immobilier.model.request.LoanApplicationRequest;
import com.example.Immobilier.Immobilier.model.request.LoanRequest;
import com.example.Immobilier.Immobilier.model.response.LoanApplicationResponseStatus;
import com.example.Immobilier.Immobilier.model.response.LoanResponse;
import com.example.Immobilier.Immobilier.service.LoanApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/loanApplication")
public class LoanApplicationController {

    @Autowired
    LoanApplicationService loanApplicationService;

    @GetMapping("/tenureList")
    public ArrayList<String> tenureList(@RequestHeader("Country") CountryEnum countryEnum , @RequestHeader("uuid") String uuid)
    {
        return loanApplicationService.tenureList(countryEnum,uuid);
    }

    @PostMapping("/addLoan")
    public ResponseEntity<LoanResponse> addLoan(@RequestBody LoanRequest loanRequest)
    {
        return new ResponseEntity<>(loanApplicationService.loanResponse(loanRequest), HttpStatus.ACCEPTED);
    }

    @PostMapping("/confirmation")
    public ResponseEntity<LoanApplicationResponseStatus> loanConfirmation(@RequestHeader("Country") CountryEnum countryEnum , @RequestHeader("uuid") String uuid , @RequestHeader SalaryRange salaryRange, @RequestBody LoanApplicationRequest loanApplicationRequest){
        return new ResponseEntity<>(loanApplicationService.loanConfirmation(countryEnum,uuid,salaryRange,loanApplicationRequest),HttpStatus.ACCEPTED);
    }

}

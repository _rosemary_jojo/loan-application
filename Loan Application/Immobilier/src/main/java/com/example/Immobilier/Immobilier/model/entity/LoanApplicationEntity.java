package com.example.Immobilier.Immobilier.model.entity;

import com.example.Immobilier.Immobilier.constant.CountryEnum;
import com.example.Immobilier.Immobilier.constant.LoanApplicationStatus;
import com.example.Immobilier.Immobilier.constant.SalaryRange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "loan_application_table")
public class LoanApplicationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Customer_id")
    public int id;
    @Column(name="Customer_name")
    public String name;
    @Column(name="Customer_age")
    public int age;
    @Column(name="National_id")
    public String national_id;
    @Column(name="Loan_amount")
    public float loan_amt;
    @Column(name="Loan_status")
    public LoanApplicationStatus status;
    @Column(name="Installment_amount")
    public float installment_amt;
    @Column(name="Interest_rate")
    public float interest_rate;
    @Column(name="Total_interest_amount")
    public float tot_interest_amt;
    @Column(name="fees")
    public float fees;
    @Column(name="total_amount")
    public float tot_amt;
    @Column(name="salary_range")
    public SalaryRange salaryRange;
    @Column(name="loan_tenure")
    public int loan_tenure;
    @Column(name="Country_name")
    public CountryEnum countryEnum;
    @Column(name="uuid")
    public String uuid;
    @Column(name="status")
    public LoanApplicationStatus loanApplicationStatus;
    @CreatedDate
    @Column(name = "Loan_Request_created_date")
    public LocalDateTime createdDate;
    @CreatedBy
    public String createBy;






}

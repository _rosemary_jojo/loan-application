package com.example.Immobilier.Immobilier.repository;

import com.example.Immobilier.Immobilier.model.entity.LoanApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoanApplicationRepository extends JpaRepository<LoanApplicationEntity,Integer> {
}

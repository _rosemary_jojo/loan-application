package com.example.Immobilier.Immobilier.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class LoanRequest {

    public int loan_tenure;
    public float loan_amt;

}

package com.example.Immobilier.Immobilier.service;

import com.example.Immobilier.Immobilier.constant.CountryEnum;
import com.example.Immobilier.Immobilier.constant.LoanApplicationStatus;
import com.example.Immobilier.Immobilier.constant.SalaryRange;
import com.example.Immobilier.Immobilier.model.entity.LoanApplicationEntity;
import com.example.Immobilier.Immobilier.model.request.LoanApplicationRequest;
import com.example.Immobilier.Immobilier.model.request.LoanRequest;
import com.example.Immobilier.Immobilier.model.response.LoanApplicationResponseStatus;
import com.example.Immobilier.Immobilier.model.response.LoanResponse;
import com.example.Immobilier.Immobilier.repository.LoanApplicationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.Immobilier.Immobilier.constant.LoanCalculationConstant.*;

@Service
@Slf4j
public class LoanApplicationService {
    @Autowired
    LoanApplicationRepository repository;
    @Autowired
    LoanCalculation loanCalculation;


    public ArrayList<String> tenureList(CountryEnum countryEnum, String uuid) {
        ArrayList<String> tenure = new ArrayList<>();
        tenure.add("12 months");
        tenure.add("24 months");
        tenure.add("36 months");
        tenure.add("48 months");
        tenure.add("60 months");
        return tenure;
    }

    public LoanResponse loanResponse(LoanRequest loanRequest) {

        float interest_rate=0;
        float fees=0;

        switch (loanRequest.getLoan_tenure()){

            case 60:
                interest_rate =SIXTY_INTEREST_RATE;
                fees =SIXTY_INTEREST_FEES;
                break;
            case 48:
                interest_rate= FORTY_EIGHT_INTEREST_RATE;
                fees=FORTY_EIGHT_INTEREST_FEES;
                break;
            case 36:
                interest_rate=THIRTY_SIX_INTEREST_RATE;
                fees=THIRTY_SIX_INTEREST_FEES;
                break;
            case 24:
                interest_rate =TWENTY_FOUR_INTEREST_RATE;
                fees=TWENTY_FOUR_INTEREST_FEES;
                break;
            case 12:
                interest_rate= TWELVE_INTEREST_RATE;
                fees=TWELVE_INTEREST_FEES;
                break;


        }

        LoanApplicationEntity loanApplicationEntity = new LoanApplicationEntity();
        float installment_amt=loanCalculation.calculateInstallment(loanRequest.getLoan_amt(),interest_rate,loanRequest.getLoan_tenure());
        float tot_amt =installment_amt * loanRequest.getLoan_tenure();
        float tot_interest_amt = (int) Math.round (tot_amt - loanRequest.getLoan_amt());
        LoanResponse loanResponse = new LoanResponse(loanRequest.getLoan_tenure(),loanRequest.getLoan_amt(),installment_amt,interest_rate,tot_interest_amt,fees,(tot_amt+fees));
        repository.save(loanApplicationEntity);
        return loanResponse;


    }

    public LoanApplicationResponseStatus loanConfirmation(CountryEnum countryEnum, String uuid, SalaryRange salaryRange, LoanApplicationRequest loanApplicationRequest) {
        float interest_rate = 0;
        float fees = 0;

        switch (loanApplicationRequest.getLoan_tenure()) {

            case 60:
                interest_rate = SIXTY_INTEREST_RATE;
                fees = SIXTY_INTEREST_FEES;
                break;
            case 48:
                interest_rate = FORTY_EIGHT_INTEREST_RATE;
                fees = FORTY_EIGHT_INTEREST_FEES;
                break;
            case 36:
                interest_rate = THIRTY_SIX_INTEREST_RATE;
                fees = THIRTY_SIX_INTEREST_FEES;
                break;
            case 24:
                interest_rate = TWENTY_FOUR_INTEREST_RATE;
                fees = TWENTY_FOUR_INTEREST_FEES;
                break;
            case 12:
                interest_rate = TWELVE_INTEREST_RATE;
                fees = TWELVE_INTEREST_FEES;
                break;
        }
        float installmentAmount = loanCalculation.calculateInstallment(loanApplicationRequest.getLoan_amt(), interest_rate, loanApplicationRequest.getLoan_tenure());
        float totalAmount = installmentAmount * loanApplicationRequest.getLoan_tenure();
        float totalInterestAmount = (int) Math.round(totalAmount - loanApplicationRequest.getLoan_amt());
        LoanApplicationEntity loanApplicationEntity = new LoanApplicationEntity();
        loanApplicationEntity.setName(loanApplicationRequest.getName());
        loanApplicationEntity.setAge(loanApplicationRequest.getAge());
        loanApplicationEntity.setNational_id(loanApplicationRequest.getNational_id());
        loanApplicationEntity.setLoan_amt(loanApplicationRequest.getLoan_amt());
        loanApplicationEntity.setLoan_tenure(loanApplicationRequest.getLoan_tenure());
        loanApplicationEntity.setInstallment_amt(installmentAmount);
        loanApplicationEntity.setTot_amt(totalAmount);
        loanApplicationEntity.setTot_interest_amt(totalInterestAmount);
        loanApplicationEntity.setInterest_rate(interest_rate);
        loanApplicationEntity.setFees(fees);
        loanApplicationEntity.setLoanApplicationStatus(LoanApplicationStatus.CONFIRMED);
        loanApplicationEntity.setUuid(uuid);
        loanApplicationEntity.setCountryEnum(countryEnum);
        loanApplicationEntity.setSalaryRange(salaryRange);
        loanApplicationEntity.setCreateBy(loanApplicationRequest.getName());
        loanApplicationEntity.setCreatedDate(LocalDateTime.now());
        LoanApplicationEntity response = repository.save(loanApplicationEntity);
        return new LoanApplicationResponseStatus(response.getId(), response.loanApplicationStatus);
    }
    }

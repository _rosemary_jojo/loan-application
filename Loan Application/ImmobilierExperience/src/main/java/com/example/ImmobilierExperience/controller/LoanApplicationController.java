package com.example.ImmobilierExperience.controller;

import com.example.ImmobilierExperience.constant.CountryEnum;
import com.example.ImmobilierExperience.constant.SalaryRange;
import com.example.ImmobilierExperience.exception.InvalidTenureException;
import com.example.ImmobilierExperience.model.request.LoanApplicationRequest;
import com.example.ImmobilierExperience.model.request.LoanRequest;
import com.example.ImmobilierExperience.model.response.LoanApplicationResponseStatus;
import com.example.ImmobilierExperience.model.response.LoanResponse;
import com.example.ImmobilierExperience.service.LoanApplicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
@RestController
@RequestMapping("/loanApplication")
@Slf4j
public class LoanApplicationController {

    @Autowired
    LoanApplicationService loanApplicationService;

    @GetMapping("/tenureList")
    public ArrayList<String> tenureList(@RequestHeader("Country") CountryEnum countryEnum, @RequestHeader("uuid") String uuid) {
        log.info("Experience tenure controller:" + uuid + ",Country" + countryEnum);
        return loanApplicationService.tenureList(countryEnum, uuid);
    }

    @PostMapping("/addLoan")
    public ResponseEntity<LoanResponse> addLoan(@RequestHeader("Country") CountryEnum countryEnum, @RequestHeader("uuid") String uuid, @RequestBody @Valid LoanRequest loanRequest) {
        try {
            log.info("Experience loan controller:" + uuid + ", Country" + countryEnum);
            return new ResponseEntity<>(loanApplicationService.loanResponse(loanRequest), HttpStatus.ACCEPTED);
        } catch (InvalidTenureException exception) {
            return new ResponseEntity(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/confirmation")
    public ResponseEntity<LoanApplicationResponseStatus> loanConfirmation(@RequestHeader("Country") CountryEnum countryEnum, @RequestHeader("uuid") String uuid, @RequestHeader SalaryRange salaryRange, @RequestBody @Valid LoanApplicationRequest loanApplicationRequest) {
        try {
            log.info("Experience confirmation controller:" + uuid + ", Country" + countryEnum);
            return new ResponseEntity<>(loanApplicationService.loanConfirmation(countryEnum, uuid, salaryRange, loanApplicationRequest), HttpStatus.ACCEPTED);
        } catch (InvalidTenureException exception) {
            return new ResponseEntity(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }
}

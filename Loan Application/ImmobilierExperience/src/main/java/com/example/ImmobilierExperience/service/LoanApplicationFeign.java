package com.example.ImmobilierExperience.service;

import com.example.ImmobilierExperience.constant.CountryEnum;
import com.example.ImmobilierExperience.constant.SalaryRange;
import com.example.ImmobilierExperience.model.request.LoanApplicationRequest;
import com.example.ImmobilierExperience.model.request.LoanRequest;
import com.example.ImmobilierExperience.model.response.LoanApplicationResponseStatus;
import com.example.ImmobilierExperience.model.response.LoanResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.ArrayList;
@FeignClient(name = "Loan-application-details", url = "${config.rest.service.getLoanManagementUrl}")
public interface LoanApplicationFeign {

    @GetMapping("/tenureList")
    ArrayList<String> getTenure(@RequestHeader("Country") CountryEnum countryEnum , @RequestHeader("uuid") String uuid);

    @PostMapping("/addLoan")
    LoanResponse postLoan(@RequestBody LoanRequest loanRequest);

    @PostMapping("/confirmation")
    LoanApplicationResponseStatus showConfirmation(@RequestHeader("Country") CountryEnum countryEnum,@RequestHeader("uuid") String uuid, @RequestHeader SalaryRange salaryRange,@RequestBody LoanApplicationRequest loanApplicationRequest);
}

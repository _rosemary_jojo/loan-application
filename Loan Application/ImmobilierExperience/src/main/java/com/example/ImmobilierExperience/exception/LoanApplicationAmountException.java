package com.example.ImmobilierExperience.exception;

public class LoanApplicationAmountException extends RuntimeException{

    public LoanApplicationAmountException(){
        super("Loan amount should be between 600 JOD and 2000 JOD");
    }
}

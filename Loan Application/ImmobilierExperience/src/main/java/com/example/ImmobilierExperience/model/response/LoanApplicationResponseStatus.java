package com.example.ImmobilierExperience.model.response;


import com.example.ImmobilierExperience.constant.LoanApplicationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanApplicationResponseStatus {

    public int id;
    public LoanApplicationStatus status;
}

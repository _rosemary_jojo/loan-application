package com.example.ImmobilierExperience.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoanRequest {

    public int loan_tenure;
    @Min(value = 600 , message = "Loan amount must be in the limit of 600 JOD to 2000 JOD")
    @Max(value = 2000 , message = "Loan amount must be in the limit of 600 JOD to 2000 JOD")
    public float loan_amt;
}

package com.example.ImmobilierExperience.constant;

public enum SalaryRange {
    JOD300_500JOD,JOD500_JOD750,JOD750_JOD1000,MORE_THAN1000JOD;
}

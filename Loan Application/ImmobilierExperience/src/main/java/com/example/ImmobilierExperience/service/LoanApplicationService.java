package com.example.ImmobilierExperience.service;

import com.example.ImmobilierExperience.constant.CountryEnum;
import com.example.ImmobilierExperience.constant.SalaryRange;
import com.example.ImmobilierExperience.exception.InvalidTenureException;
import com.example.ImmobilierExperience.exception.LoanApplicationAmountException;
import com.example.ImmobilierExperience.model.request.LoanApplicationRequest;
import com.example.ImmobilierExperience.model.request.LoanRequest;
import com.example.ImmobilierExperience.model.response.LoanApplicationResponseStatus;
import com.example.ImmobilierExperience.model.response.LoanResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
@Service
public class LoanApplicationService {
    @Autowired
    LoanApplicationFeign loanApplicationFeign;
    public ArrayList<String> tenureList(CountryEnum countryEnum, String uuid) {
        return loanApplicationFeign.getTenure(countryEnum,uuid);
    }

    public LoanResponse loanResponse(LoanRequest loanRequest) {
        if(loanRequest.getLoan_amt() >599 || loanRequest.getLoan_amt()<2001){
            tenureCheck(loanRequest.getLoan_tenure());
            return loanApplicationFeign.postLoan(loanRequest);
        }
        else{
            throw new LoanApplicationAmountException();
        }

    }

    public LoanApplicationResponseStatus loanConfirmation(CountryEnum countryEnum, String uuid, SalaryRange salaryRange, LoanApplicationRequest loanApplicationRequest) {
        tenureCheck(loanApplicationRequest.getLoan_tenure());
        return loanApplicationFeign.showConfirmation(countryEnum,uuid,salaryRange,loanApplicationRequest);
    }


























    public void tenureCheck(int tenure){
        if(tenure ==12 || tenure ==24 || tenure ==36 || tenure ==48 || tenure == 60){

        }
        else {
            throw new InvalidTenureException();
        }
    }
}

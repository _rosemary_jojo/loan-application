package com.example.ImmobilierExperience;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ImmobilierExperienceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImmobilierExperienceApplication.class, args);
	}

}

package com.example.ImmobilierExperience.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanApplicationRequest {

    @Size(min = 5 , max = 30 , message = "Enter Name with size between 5 & 30")
    public String name;
    public int age;
    @Pattern(regexp = "^\\d{10}$" , message = "The National Id must be 10 digits.")
    public String national_id;
    @Min(value = 600 , message = "Loan amount must be in the limit of 600 JOD to 2000 JOD")
    @Max(value = 2000 , message = "Loan amount must be in the limit of 600 JOD to 2000 JOD")
    public float loan_amt;
    public int loan_tenure;

}

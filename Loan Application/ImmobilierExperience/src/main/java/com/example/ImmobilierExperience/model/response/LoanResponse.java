package com.example.ImmobilierExperience.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoanResponse {
    public int loan_tenure;
    public float loan_amt;
    public float installment_amt;
    public float interest_rate;
    public float tot_interest_amt;
    public float fees;
    public float tot_amt;

}
